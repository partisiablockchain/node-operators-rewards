# node-operators-rewards

This repository contains the list of node operators and their corresponding rewards for each
period.

The maven project inside this repository is used to calculate the different rewards.  

## Mainnet rewards
Files are in [mainnet](mainnet/README.md).

See this directory for more information.

## Pre-mainnet rewards

Files are in [pre-mainnet](pre-mainnet/README.md).

See this directory for more information of how the 1,103,777.5993 tokens was distributed.
