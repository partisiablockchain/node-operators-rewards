# Mainnet rewards

For each quarter a after mainnet launch, a fixed amount of tokens are allocated for rewards. The
amounts can be found in [reward-periods.csv](reward-periods.csv). This document describes how the
rewards are distributed.

Description of the on-chain MPC tokens model can be found
in [Partisia Blockchain documentation](https://partisiablockchain.gitlab.io/documentation/pbc-fundamentals/mpc-token-model-and-account-elements.html).

## Computing rewards

Rewards to token holder i (x_i) is computed as: x_i = X * (i’s rewardable tokens * node
performance) / (sum of all rewardable tokens * node performance).

Where:

* “X” is the total allocated rewards to a given period
* “i’s rewardable tokens” are all delegated tokens * (unlocked tokens/All tokens) from token holder
  i delegated to one or more node accounts
* “Delegated tokens” are tokens delegated to other node operators or to the token holder’s own node
  operator account.
* “Node performance” is the “performance score” of the nodes that token holder i has staked tokens
  to.
* “Performance score” is a node's relative performance initially measured in the relative number of
  blocks produced.
* “Sum of all rewardable tokens * node performance” is the total amount of rewardable tokens
  adjusted for the node performance in a given time period.

Time is defined when data is extracted from the public ledger. The current use of time is:
* Node performance is measured monthly by the number of blocks produced by a given node operator in
  a given month relative to the best performing node operator in that given month.
* Delegated staking is measured in full weeks captured every Sunday 23:59:59 UTC.
* Rewards are computed once per week.
* X is the number of tokens allocated to a given quarter. X is uniformly distributed over the weeks
  in a given quarter. The number of tokens per quarter is fixed a priori and publicly announced.

An instance of all the below files are created per week. The data is for Sunday 23:59:59 UTC.

### Definitions in the Producer file

Contains the producer committee that was active at the time of export.

Information found on-chain:

* #Identity: BlockchainAddress of the producer

### Definitions in the TokenHolders file

Contains every account that has either Staking to self or Delegated Staking. Every entry might be
eligible for rewards.

Information found on-chain

* #address: Token holder's account (address)

MPC Token Amounts (see section “MPC Model” for the computation from on-chain information):

* #Transferrable
* #InTransit
* #StakedToSelf
* #PendingUnstakes
* #DelegatedToOthers
* #AcceptedFromOthers
* #StakeForJobs
* #PendingRetractedDelegated
* #InSchedule
* #Releasable
  * When exporting data prior to 30. November 2022 the releasable tokens are calculated based
    on 30. November 2022 at 23:59:59 as no tokens were released prior to this.
* #Staked
* #Unlocked
* #Total

Computed for rewards (from MPC Token Amounts)

* #UnlockedRatio: The percentage of MPC tokens that are unlocked relative to the all of the user’s
  MPC tokens
  * #UnlockedRatio = #Unlocked / #Total
* #StakeRatio: The percentage of MPC tokens that are staked
  * #StakeRatio = #Staked / #Total
* #StakeCapRatio: The percentage that rewards should be decreased with due to more than 5 million
  staked tokens on this node
  * #StakeCapRatio = min( 50000000000 / #StakeForJobs, 1 )

### Definitions in the Stakes file

Contains the list of stakes, either stake delegation or own stakes. For own stakes the from and to
are the same and PendingAmount is set to 0. Stake delegations that has expired are not included in
the list.

Information found on-chain

* #From: The sender account of delegated staking
* #To: The receiver account of the delegated staking
* #Amount: The number of MPC tokens that has been delegated and accepted
* #PendingAmount: The number of MPC tokens that has been delegated and are pending accept

Variables from TokenHolders file for #From

* #Staked
* #Unlocked
* #Total
* #UnlockedRatio
* #StakeRatio

Variables from TokenHolders file for #To

* #StakeCapRatio

Variable from Other Source

* #BpScore: Node Operator performance score for defined by number of blocks produced in a given
  calendar month/best practice number of blocks produced in a given calendar month.

Computed variables

* #DelegationRatio: The percentage that this delegation represents in relation to the the total
  stakes for #From
  * #DelegationRatio = #Amount / #Total
* #RewardableBeforePerformance: The number of tokens that are rewardable prior to performance
  adjustment
  * #RewardableBeforePerformance = #Total * #UnlockedRatio * #StakeRatio * #DelegationRatio *
    #StakeCapRatio
* #Rewardable: The number of tokens that are rewardable after performance adjustment
  * #Rewardable = #RewardableBeforePerformance * #BpScore
* RewardableTokenHolder: The number of tokens that should be rewarded to #From
  * #RewardableTokenHolder = #Rewardable * 0.98
* #RewardableBp: The number of tokens that should be rewarded to #To
  * #RewardableBp = #Rewardable * 0.02

### Definitions in the Rewards file

Contains a list of identities that are to receive rewards.

Variable from Other Source

* #WeeklyRewardsInQuarter
  * The number of tokens that is to be distributed per week in the export quarter

Aggregates from Stakes file

* #Identity
  * Each unique #From
* #SumOfRewardable
  * Sum(#Rewardable)
* #RewardableForHolder
  * Sum(#RewardableTokenHolder where #From == #Identity) + Sum(#RewardableBp where #To == #Identity)
* #Rewards
  * #RewardableForHolder / #SumOfRewardable * #WeeklyRewardsInQuarter

## Performance scores

Score for block producers was for the first 9 months calculated using the R scripts
in [Scripts and definitions](Scripts%20and%20definitions) based on the data
in [LedgerData used for node performance](LedgerData%20used%20for%20node%20performance). Later
months are calculated using BpScore.java.   
The results of these scripts are in [Performance scores](results%2FPerformance%20scores).

From Q10 the performance scores are part of the files in Results.

## Results

From Q10 the reward computation are handled by each node in the network using code
in [partisiablockchain/main](https://gitlab.com/partisiablockchain/main). See
also [documentation](https://partisiablockchain.gitlab.io/documentation/node-operations/node-health-and-maintenance.html#voting-for-quarterly-rewards).

For each quarter a directory is created in [results](results) that contains the exports defined in
the definitions document. The final rewards for the quarter (calculated only for finished quarters)
are inside the result directories. For the first quarters these are:

- [Q1](results%2FRewards%20Q1%2Fsign-rewards-Q1.txt)
- [Q2](results%2FRewards%20Q2%2Fsign-rewards-Q2.txt)
- [Q3](results%2FRewards%20Q3%2Fsign-rewards-Q3.txt)
- [Q4](results%2FRewards%20Q4%2Fsign-rewards-Q4.txt)
- [Q5](results%2FRewards%20Q5%2Fsign-rewards-Q5.txt)
- [Q6](results%2FRewards%20Q6%2Fsign-rewards-Q6.txt)
- [Q7](results%2FRewards%20Q7%2Fsign-rewards-Q7.txt)
- [Q8](results%2FRewards%20Q8%2Fsign-rewards-Q8.txt)
- [Q9](results%2FRewards%20Q9%2Fsign-rewards-Q9.txt)
- [Q10](results%2FRewards%20Q10%2Fsign-rewards-Q10.txt)

The format for the rewards are:

1. Address
2. Amount
3. ReleaseDuration
4. ReleaseInterval
5. ReleaseTime

The parameters used for ReleaseDuration, ReleaseInterval and ReleaseTime makes the tokens releasable
15 days after the quarter ended.