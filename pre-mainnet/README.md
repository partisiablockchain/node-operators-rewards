# node-operators-rewards

This repository contains the list of node operators and their corresponding rewards for each
period.

## Pre-mainnet rewards

The script for generating the rewards is in [premainnet.R](pre-mainnet/premainnet.R)

Input are the files R0, R1, R2, R3, main which is the committee members with their stake.

Output are: Reward0, Reward1, Reward2, Reward3.
Added are also the two stat files: NodeStat and Overview.

The rewards are paid out in four installments

- [R0](Sign-rewards-0.txt)
- [R1](Sign-rewards-1.txt)
- [R2](Sign-rewards-2.txt)
- [R3](Sign-rewards-3.txt)

The format for the rewards are:

1. Address
2. Amount
3. ReleaseDuration: PT17531H38M24S corresponds to 2 years
4. ReleaseInterval: PT2191H27M18S corresponds to 3 months
5. ReleaseTime

The parameters, PT17531H38M24S PT2191H27M18S 2022-08-31T00:00:00Z, means that the tokens are
released for the first time at 2022-11-30T07:27:18Z and for the last time at 2024-08-30T11:38:24Z.

## Re-calculated pre-mainnet rewards

The pre-mainnet rewards has been re-calculated based on "total number of tokens" as opposed to
"number of staked tokens". Although reward is in fact based on staked tokens, staking was set to the
default minimum number of stakes during pre-mainnet. Not all was aware of this and changed the
stakes to the preferred level. Delegating stakes is the responsibility of the node operators, but
for the pre-mainnet, we have chosen to use “total number of tokens” as stakes. Hereby everyone get
the most out of the pre-mainnet rewards.

Also, a single node operator account has been updated.

You can find the updated rewards here: [Reward0.021122.csv](Reward0.021122.csv)
, [Reward1.021122.csv](Reward1.021122.csv), [Reward2.021122.csv](Reward2.021122.csv)
, [Reward3.021122.csv](Reward3.021122.csv)

NB. Any feedback should be addressed in a direct message to \@Kristian Hu#7382 as soon as possible
and no later than November 5th.
