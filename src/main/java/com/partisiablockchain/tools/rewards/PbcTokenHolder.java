package com.partisiablockchain.tools.rewards;

import com.partisiablockchain.tools.rewards.export.TokenHolder;
import java.util.Map;

/** Core data available about a token holder in the account plugin. */
public record PbcTokenHolder(
    String address,
    long transferable,
    long inTransit,
    long stakedToSelf,
    long pendingUnstakes,
    long delegatedToOthers,
    long pendingRetractedDelegated,
    long inSchedule,
    long releaseable,
    Map<String, IncomingStakes> receivedDelegations) {

  TokenHolder createTokenHolder() {
    long staked = stakedToSelf() + delegatedToOthers();
    long pendingUnused = pendingUnstakes() + pendingRetractedDelegated();
    long inUse = staked + pendingUnused + inTransit();
    long unlocked = inUse + transferable() + releaseable();
    long released = inUse + transferable();
    long total = released + inSchedule();
    long acceptedFromOthers =
        receivedDelegations.values().stream().mapToLong(IncomingStakes::accepted).sum();
    long stakeForJobs = acceptedFromOthers + stakedToSelf();
    double stakeCapRatio = Math.min(5_000_000_0000d / stakeForJobs, 1d);
    return new TokenHolder(
        address(),
        transferable(),
        inTransit(),
        stakedToSelf(),
        pendingUnstakes(),
        delegatedToOthers(),
        acceptedFromOthers,
        stakeForJobs,
        pendingRetractedDelegated(),
        inSchedule(),
        releaseable(),
        staked,
        unlocked,
        total,
        unlocked * 1.0d / total,
        staked * 1.0d / total,
        stakeCapRatio);
  }

  /** Delegated stakes received from another account. */
  public record IncomingStakes(long pending, long accepted) {}
}
