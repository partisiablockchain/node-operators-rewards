package com.partisiablockchain.tools.rewards.export;

/** A row in the Producer csv export. */
public record Producer(String identity) {}
