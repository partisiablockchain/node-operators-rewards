package com.partisiablockchain.tools.rewards.export;

/** A row in the Rewards csv export file. */
public record Rewards(
    String identity,
    long weeklyRewardInQuarter,
    double sumOfRewardable,
    double rewardableForHolder,
    double rewards) {}
