package com.partisiablockchain.tools.rewards.export;

/** Core data and aggregates used for TokenHolder csv export. */
public record TokenHolder(
    String address,
    long transferable,
    long inTransit,
    long stakedToSelf,
    long pendingUnstakes,
    long delegatedToOthers,
    long acceptedFromOthers,
    long stakeForJobs,
    long pendingRetractedDelegated,
    long inSchedule,
    long releaseable,
    long staked,
    long unlocked,
    long total,
    double unlockedRatio,
    double stakeRatio,
    double stakeCapRatio) {}
