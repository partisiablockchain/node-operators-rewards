package com.partisiablockchain.tools.rewards.export;

/** A row in the Stakes csv export file. */
public record Stakes(
    String from,
    String to,
    long amount,
    long pendingAmount,
    long staked,
    long unlocked,
    long total,
    double unlockedRatio,
    double stakeRatio,
    double stakeCapRatio,
    double bpScore,
    double delegationRatio,
    double rewardableBeforePerformance,
    double rewardable,
    double rewardableTokenHolder,
    double rewardableBp) {}
