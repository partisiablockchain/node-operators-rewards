package com.partisiablockchain.tools.rewards;

import com.fasterxml.jackson.databind.JsonNode;
import com.partisiablockchain.client.JerseyWebClient;
import com.partisiablockchain.client.WebClient;
import com.partisiablockchain.dto.ContractState;
import com.partisiablockchain.dto.traversal.TraversePath;
import com.partisiablockchain.tools.rest.RestResources;
import com.partisiablockchain.tools.rewards.export.Producer;
import com.partisiablockchain.tools.rewards.export.Rewards;
import com.partisiablockchain.tools.rewards.export.Stakes;
import com.partisiablockchain.tools.rewards.export.TokenHolder;
import com.secata.tools.coverage.ExceptionConverter;
import jakarta.ws.rs.client.ClientBuilder;
import java.io.File;
import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.RecordComponent;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.StringJoiner;
import java.util.concurrent.TimeUnit;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * Utility for exporting stakes of all block producers and calculating rewards for all stakes. If
 * maven is installed it can be executed using <code>
 * mvn compile exec:java -Dexec.mainClass="com.partisiablockchain.tools.rewards.ExportStakes"</code>
 */
public class ExportStakes {
  private static final long INITIAL_RELEASE = Instant.parse("2022-11-30T23:59:59Z").toEpochMilli();
  private final long time;
  private final Map<String, PbcTokenHolder> tokenHolderMap;
  private final String format;
  private static final WebClient webClient;

  static {
    webClient =
        new JerseyWebClient(
            ClientBuilder.newBuilder()
                .connectTimeout(30L, TimeUnit.SECONDS)
                .readTimeout(30L, TimeUnit.SECONDS)
                .withConfig(new ResourceConfig().register(RestResources.DEFAULT))
                .build());
  }

  private final Map<String, Double> bpScore;
  private final long weeklyRewardInQuarter;
  private final File directory;

  ExportStakes(
      Map<String, Double> bpScore,
      File outputDirectory,
      long weeklyRewardInQuarter,
      LocalDate sunday) {
    this.bpScore = bpScore;
    this.weeklyRewardInQuarter = weeklyRewardInQuarter;
    LocalDateTime from = sunday.atTime(LocalTime.MAX);
    format =
        DateTimeFormatter.ofPattern("yyyy-'W'ww").withZone(ZoneId.systemDefault()).format(from);
    System.out.println(format);
    this.time = from.toInstant(ZoneOffset.UTC).toEpochMilli();
    tokenHolderMap = new HashMap<>();
    this.directory = outputDirectory;
  }

  /**
   * Export rewards for a period.
   *
   * @param args [rewardsQuarter] or empty to read from stdin
   * @throws Exception on error
   */
  public static void main(String[] args) throws Exception {
    String[] actualArgs;
    if (args.length == 0) {
      actualArgs = new String[1];
      Scanner scanner = new Scanner(System.in, StandardCharsets.UTF_8);
      System.out.println("Input quarter to export");
      actualArgs[0] = scanner.nextLine();
    } else {
      actualArgs = args;
    }
    LocalDate firstMonth = LocalDate.of(2022, Month.JUNE, 1);

    int quarter = Integer.parseInt(actualArgs[0]);

    final File outputDirectory = new File("mainnet/results/Rewards Q" + quarter);
    ExceptionConverter.run(() -> Files.createDirectories(outputDirectory.toPath()));
    final File rewardsFile = new File(outputDirectory, "sign-rewards-Q" + quarter + ".txt");
    if (rewardsFile.exists()) {
      System.out.println("Rewards file already exist. Ignoring export.");
      return;
    }

    List<LocalDate> sundays = determineSundays(firstMonth, quarter);
    long rewardsInPeriod =
        Long.parseLong(
            Files.readAllLines(Path.of("mainnet/reward-periods.csv"))
                .get(quarter)
                .split(";", -1)[1]);
    long weeklyRewardInQuarter = rewardsInPeriod * 10_000L / sundays.size();

    System.out.println(
        "Exporting stakes in quarter "
            + quarter
            + ". Weekly rewards "
            + weeklyRewardInQuarter
            + ".");

    final Map<Long, Map<String, Double>> monthlyBpScores = new HashMap<>();

    Map<String, Double> rewardsForSignature = new HashMap<>();
    boolean incompleteExport = false;
    for (LocalDate sunday : sundays) {
      LocalDate yesterday = LocalDate.now(ZoneOffset.UTC).minusDays(1);
      if (sunday.isBefore(yesterday)) {
        long bpScoreMonth = firstMonth.until(sunday, ChronoUnit.MONTHS) + 1;
        Map<String, Double> bpScore =
            monthlyBpScores.computeIfAbsent(bpScoreMonth, ExportStakes::readBpScores);

        ExportStakes exportStakes =
            new ExportStakes(bpScore, outputDirectory, weeklyRewardInQuarter, sunday);
        List<Rewards> weeklyRewards = exportStakes.exportWeek();

        for (Rewards weeklyReward : weeklyRewards) {
          double currentSum = rewardsForSignature.getOrDefault(weeklyReward.identity(), 0d);
          rewardsForSignature.put(weeklyReward.identity(), currentSum + weeklyReward.rewards());
        }
      } else {
        System.err.println("Ignoring week in the future ending at: " + sunday);
        incompleteExport = true;
      }
    }

    if (incompleteExport) {
      System.out.println("The quarter has not yet ended, no sign file is generated");
    } else {
      // Release time is set to the day before the unlock pause was enabled to make them releasable
      Instant releaseTime = Instant.parse("2023-05-19T00:00:00Z");
      String tge = "2022-08-31T00:00:00Z";
      Duration releaseDuration = Duration.between(Instant.parse(tge), releaseTime);
      long totalReward = 0;
      PrintStream rewards = new PrintStream(rewardsFile);
      List<String> sortedKeys = rewardsForSignature.keySet().stream().sorted().toList();
      for (String identity : sortedKeys) {
        double rewardWithDecimals = rewardsForSignature.get(identity);
        long reward = (long) rewardWithDecimals;
        if (reward > 0) {
          rewards.println(
              identity
                  + " "
                  + BigDecimal.valueOf(reward, 4)
                  + " "
                  + releaseDuration
                  + " "
                  + releaseDuration
                  + " "
                  + tge);
          totalReward += reward;
        }
      }
      System.out.println("Total reward for export: " + BigDecimal.valueOf(totalReward, 4));
    }
  }

  private static List<LocalDate> determineSundays(LocalDate firstMonth, int quarter) {
    LocalDate first =
        firstMonth
            .plusMonths((quarter - 1L) * 3)
            .with(TemporalAdjusters.firstInMonth(DayOfWeek.SUNDAY));

    LocalDate last = first.plusMonths(2).with(TemporalAdjusters.firstDayOfNextMonth());
    List<LocalDate> sundays = new ArrayList<>();
    LocalDate next = first;
    while (next.isBefore(last)) {
      sundays.add(next);
      next = next.with(TemporalAdjusters.next(DayOfWeek.SUNDAY));
    }
    return sundays;
  }

  private static Map<String, Double> readBpScores(Long forMonth) {
    ExceptionConverter.run(() -> BpScore.calculateScoreForMonth(forMonth));

    List<String> lines =
        ExceptionConverter.call(
            () ->
                Files.readAllLines(
                    Path.of("mainnet/results/Performance scores", "m" + forMonth + "_score.csv")));

    Map<String, Double> scores = new HashMap<>();
    for (int i = 1; i < lines.size(); i++) {
      String line = lines.get(i);
      String[] split = line.split(";", -1);
      String address = split[0];
      String score = split[2];
      double parsedScore = Double.parseDouble(score);
      if (parsedScore > 1) {
        throw new RuntimeException("Unexpected score " + parsedScore);
      }
      scores.put(address, parsedScore);
    }
    return Collections.unmodifiableMap(scores);
  }

  private List<Rewards> exportWeek() throws Exception {
    if (exportFile(Rewards.class).exists()) {
      System.out.println("Reading existing rewards from file for week " + format);
      return readExport(Rewards.class);
    } else {
      List<String> shards = List.of("Gov", "Shard0", "Shard1", "Shard2");
      shards.forEach(this::buildTokenHolder);
      List<Producer> producers = getCommittee();
      Map<String, TokenHolder> tokenHolderFile = buildTokenHolderContent();
      List<Stakes> stakes = buildStakes(producers, tokenHolderFile);
      print(Producer.class, producers);
      print(Stakes.class, stakes);
      print(TokenHolder.class, tokenHolderFile.values());
      List<Rewards> rewards = buildRewards(stakes);
      print(Rewards.class, rewards);
      return rewards;
    }
  }

  private <T extends Record> List<T> readExport(Class<T> rewardsClass) throws Exception {
    List<String> strings = Files.readAllLines(exportFile(rewardsClass).toPath());
    List<T> rewards = new ArrayList<>();
    RecordComponent[] recordComponents = rewardsClass.getRecordComponents();
    Constructor<T> recordConstructor =
        rewardsClass.getDeclaredConstructor(
            Arrays.stream(recordComponents).map(RecordComponent::getType).toArray(Class[]::new));
    for (String s : strings.subList(1, strings.size())) {
      String[] values = s.split(";", -1);
      Object[] constructorArguments = new Object[recordComponents.length];
      for (int i = 0; i < recordComponents.length; i++) {
        Class<?> type = recordComponents[i].getType();
        String value = values[i];
        if (type.equals(String.class)) {
          constructorArguments[i] = value;
        } else if (type.equals(Long.TYPE)) {
          constructorArguments[i] = Long.parseLong(value);
        } else if (type.equals(Double.TYPE)) {
          constructorArguments[i] = Double.parseDouble(value);
        } else {
          throw new RuntimeException("Unexpected type when reading export");
        }
      }
      rewards.add(recordConstructor.newInstance(constructorArguments));
    }
    return rewards;
  }

  private <T extends Record> void print(Class<T> recordType, Collection<T> entries)
      throws Exception {
    RecordComponent[] recordComponents = recordType.getRecordComponents();
    File exportFile = exportFile(recordType);
    PrintStream nodeOperatorsFile = new PrintStream(exportFile);

    StringJoiner header = new StringJoiner(";");
    for (RecordComponent recordComponent : recordComponents) {
      String name = recordComponent.getName();
      header.add(name.substring(0, 1).toUpperCase() + name.substring(1));
    }
    nodeOperatorsFile.println(header);

    List<T> sortedEntries = new ArrayList<>(entries);
    sortedEntries.sort(
        Comparator.comparing(
            t ->
                ExceptionConverter.call(
                    () -> String.valueOf(recordComponents[0].getAccessor().invoke(t)))));

    for (T entry : sortedEntries) {
      StringJoiner line = new StringJoiner(";");
      for (RecordComponent recordComponent : recordComponents) {
        Object value = recordComponent.getAccessor().invoke(entry);
        line.add(String.valueOf(value));
      }
      nodeOperatorsFile.println(line);
    }
  }

  private <T> File exportFile(Class<T> recordType) {
    String simpleName = recordType.getSimpleName();
    return new File(directory, "export-" + format + "-" + simpleName + ".csv");
  }

  private List<Rewards> buildRewards(List<Stakes> stakes) {
    double sumOfRewardable = stakes.stream().mapToDouble(Stakes::rewardable).sum();

    Map<String, Double> rewardable = new HashMap<>();
    for (Stakes stake : stakes) {
      Double currentSum = rewardable.getOrDefault(stake.from(), 0d);
      rewardable.put(stake.from(), currentSum + stake.rewardableTokenHolder());

      Double bpSum = rewardable.getOrDefault(stake.to(), 0d);
      rewardable.put(stake.to(), bpSum + stake.rewardableBp());
    }
    List<Rewards> weeklyRewards = new ArrayList<>();
    for (String identity : rewardable.keySet()) {
      double rewardableForHolder = rewardable.get(identity);
      double rewards = rewardableForHolder / sumOfRewardable * weeklyRewardInQuarter;
      weeklyRewards.add(
          new Rewards(
              identity, weeklyRewardInQuarter, sumOfRewardable, rewardableForHolder, rewards));
    }
    return weeklyRewards;
  }

  private List<Stakes> buildStakes(
      List<Producer> producers, Map<String, TokenHolder> tokenHolderFile) {
    List<Stakes> result = new ArrayList<>();
    for (Producer producer : producers) {
      String operator = producer.identity();
      Double bpScore = this.bpScore.get(operator);
      if (bpScore == null) {
        System.err.println("No BP score for " + operator);
        bpScore = 0d;
      }

      PbcTokenHolder operatorAccount = tokenHolderMap.get(operator);
      if (operatorAccount != null) {
        final double stakeCapRatio = tokenHolderFile.get(operator).stakeCapRatio();
        Map<String, PbcTokenHolder.IncomingStakes> delegations =
            operatorAccount.receivedDelegations();
        for (String from : delegations.keySet()) {
          PbcTokenHolder.IncomingStakes incomingStakes = delegations.get(from);
          TokenHolder holder = tokenHolderFile.get(from);
          Stakes stakes =
              createStakes(operator, stakeCapRatio, bpScore, from, incomingStakes, holder);
          result.add(stakes);
        }
        if (operatorAccount.stakedToSelf() > 0) {
          result.add(
              createStakes(
                  operator,
                  stakeCapRatio,
                  bpScore,
                  operator,
                  new PbcTokenHolder.IncomingStakes(0, operatorAccount.stakedToSelf()),
                  tokenHolderFile.get(operator)));
        }
      } else {
        System.err.println("No account for producer: " + operator);
      }
    }
    return result;
  }

  private static Stakes createStakes(
      String operator,
      double stakeCapRatio,
      double bpScore,
      String from,
      PbcTokenHolder.IncomingStakes incomingStakes,
      TokenHolder holder) {
    long amount = incomingStakes.accepted();
    long totalStakes = holder.staked();
    double delegationRatio = amount * 1.0 / totalStakes;
    long totalTokens = holder.total();
    double rewardableBeforePerformance =
        totalTokens
            * holder.unlockedRatio()
            * holder.stakeRatio()
            * delegationRatio
            * stakeCapRatio;
    double rewardable = rewardableBeforePerformance * bpScore;
    double rewardableTokenHolder = rewardable * 0.98;
    double rewardableBp = rewardable * 0.02;
    return new Stakes(
        from,
        operator,
        amount,
        incomingStakes.pending(),
        totalStakes,
        holder.unlocked(),
        totalTokens,
        holder.unlockedRatio(),
        holder.stakeRatio(),
        stakeCapRatio,
        bpScore,
        delegationRatio,
        rewardableBeforePerformance,
        rewardable,
        rewardableTokenHolder,
        rewardableBp);
  }

  private Map<String, TokenHolder> buildTokenHolderContent() {
    Map<String, TokenHolder> tokenHolders = new HashMap<>();
    for (String account : tokenHolderMap.keySet()) {
      PbcTokenHolder pbcTokenHolder = tokenHolderMap.get(account);
      tokenHolders.put(account, pbcTokenHolder.createTokenHolder());
    }
    return tokenHolders;
  }

  private void buildTokenHolder(String shard) {
    JsonNode accountPlugin = getAccountPlugin(shard);
    accountPlugin = accountPlugin.get("accounts");
    accountPlugin.iterator().forEachRemaining(this::buildTokenHolderAccount);
  }

  private void buildTokenHolderAccount(JsonNode jsonNode) {
    String address = jsonNode.get("key").textValue();
    jsonNode = jsonNode.get("value");
    long stakedToSelf = parseLong(jsonNode.get("stakedTokens"), 0);

    long delegatedToOthers = 0;
    JsonNode delegatedStakesToOthersNode = jsonNode.get("delegatedStakesToOthers");
    if (delegatedStakesToOthersNode != null) {
      for (JsonNode next : delegatedStakesToOthersNode) {
        JsonNode value = next.get("value");
        long toOther = Long.parseLong(value.asText());
        delegatedToOthers += toOther;
      }
    }

    Map<String, PbcTokenHolder.IncomingStakes> delegatedStakesFromOthers = new HashMap<>();
    JsonNode delegatedStakesFromOthersNode = jsonNode.get("delegatedStakesFromOthers");
    if (delegatedStakesFromOthersNode != null) {
      for (JsonNode next : delegatedStakesFromOthersNode) {
        String other = next.get("key").textValue();
        long accepted = Long.parseLong(next.get("value").get("acceptedDelegatedStakes").asText());
        long pending = Long.parseLong(next.get("value").get("pendingDelegatedStakes").asText());
        delegatedStakesFromOthers.put(other, new PbcTokenHolder.IncomingStakes(pending, accepted));
      }
    }
    if (stakedToSelf + delegatedToOthers > 0 || !delegatedStakesFromOthers.isEmpty()) {
      long pendingRetractedDelegated = 0;
      JsonNode pendingRetractedDelegatedStakes = jsonNode.get("pendingRetractedDelegatedStakes");
      if (pendingRetractedDelegatedStakes != null) {
        for (JsonNode next : pendingRetractedDelegatedStakes) {
          JsonNode value = next.get("value");
          pendingRetractedDelegated += Long.parseLong(value.asText());
        }
      }
      long pendingUnstakes = 0;
      JsonNode pendingUnstakesNode = jsonNode.get("pendingUnstakes");
      if (pendingUnstakesNode != null) {
        for (JsonNode next : pendingUnstakesNode) {
          JsonNode value = next.get("value");
          pendingUnstakes += Long.parseLong(value.asText());
        }
      }

      long transferable = parseLong(jsonNode.get("mpcTokens"), 0);
      long computeInStakingTransit = computeInStakingTransit(jsonNode);
      long computeInTransferTransit = computeInTransferTransit(jsonNode);
      long inTransit = computeInStakingTransit + computeInTransferTransit;

      // Aggregates
      long releaseAble = 0;
      long inSchedule = 0;
      for (JsonNode vestingAccount : jsonNode.get("vestingAccounts")) {
        long amountToRelease = getAmountToRelease(vestingAccount);
        releaseAble += amountToRelease;
        inSchedule +=
            parseLong(vestingAccount.get("tokens"))
                - parseLong(vestingAccount.get("releasedTokens"));
      }

      PbcTokenHolder value =
          new PbcTokenHolder(
              address,
              transferable,
              inTransit,
              stakedToSelf,
              pendingUnstakes,
              delegatedToOthers,
              pendingRetractedDelegated,
              inSchedule,
              releaseAble,
              delegatedStakesFromOthers);

      tokenHolderMap.put(address, value);
    }
  }

  private long computeInStakingTransit(JsonNode jsonNode) {
    long pendingStakeDelegations = 0;
    JsonNode storedPendingStakeDelegations = jsonNode.get("storedPendingStakeDelegations");
    if (storedPendingStakeDelegations != null) {
      for (JsonNode pendingTransfer : storedPendingStakeDelegations) {
        JsonNode value = pendingTransfer.get("value");
        String delegationType = value.get("delegationType").asText();
        boolean isDelegateStakesOrRetractDelegatedStakes =
            delegationType.equals("DELEGATE_STAKES")
                || delegationType.equals("RETRACT_DELEGATED_STAKES");
        if (isDelegateStakesOrRetractDelegatedStakes) {
          pendingStakeDelegations += parseLong(value.get("amount"));
        }
      }
    }
    return pendingStakeDelegations;
  }

  private long computeInTransferTransit(JsonNode jsonNode) {
    long pendingTransfers = 0;
    JsonNode storedPendingTransfers = jsonNode.get("storedPendingTransfers");
    if (storedPendingTransfers != null) {
      for (JsonNode pendingTransfer : storedPendingTransfers) {

        JsonNode value = pendingTransfer.get("value");
        if (!value.get("addTokensOrCoinsIfTransferSuccessful").asBoolean()
            && parseLong(value.get("coinIndex")) == -1) {
          pendingTransfers += parseLong(value.get("amount"));
        }
      }
    }
    return pendingTransfers;
  }

  long getAmountToRelease(JsonNode vestingAccount) {
    boolean beforeFirstRelease = time < INITIAL_RELEASE;
    long releaseTime = beforeFirstRelease ? INITIAL_RELEASE : time;

    long releaseDuration = parseLong(vestingAccount.get("releaseDuration"));
    long releaseInterval = parseLong(vestingAccount.get("releaseInterval"));
    long releasedTokens = parseLong(vestingAccount.get("releasedTokens"));
    long tokenGenerationEvent = parseLong(vestingAccount.get("tokenGenerationEvent"));
    long tokens = parseLong(vestingAccount.get("tokens"));

    VestingScheduleCalculator vestingScheduleCalculator =
        new VestingScheduleCalculator(
            tokens, releasedTokens, tokenGenerationEvent, releaseDuration, releaseInterval);
    return vestingScheduleCalculator.getAmountToRelease(releaseTime);
  }

  private long parseLong(JsonNode stakedTokens, int defaultValue) {
    if (stakedTokens != null && !stakedTokens.isNull()) {
      return parseLong(stakedTokens);
    } else {
      return defaultValue;
    }
  }

  private static long parseLong(JsonNode node) {
    return Long.parseLong(node.asText());
  }

  private static String url(String shard) {
    if ("Gov".equals(shard)) {
      return "https://reader.partisiablockchain.com";
    } else {
      return "https://reader.partisiablockchain.com/shards/" + shard;
    }
  }

  private JsonNode getAccountPlugin(String shard) {
    long latestBlockBeforeTime = getLatestBlockTimeBefore(shard, time);
    String baseUrl = url(shard) + "/blockchain";
    return webClient.post(
        baseUrl + "/accountPlugin/local" + "?blockTime=" + latestBlockBeforeTime,
        new TraversePath(List.of()),
        JsonNode.class);
  }

  private List<Producer> getCommittee() {
    long latestBlockBeforeTime = getLatestBlockTimeBefore("Shard0", time);
    String baseUrl = url("Shard0") + "/blockchain";
    ContractState contractState =
        webClient.get(
            baseUrl
                + "/contracts/04203b77743ad0ca831df9430a6be515195733ad91"
                + "?blockTime="
                + latestBlockBeforeTime,
            ContractState.class);
    List<Producer> committee = new ArrayList<>();
    JsonNode serializedContract = contractState.serializedContract();
    for (JsonNode member : serializedContract.get("committee")) {
      committee.add(new Producer(member.get("identity").textValue()));
    }
    return committee;
  }

  static long getLatestBlockTimeBefore(String shard, long timestamp) {
    String baseUrl = url(shard) + "/blockchain";
    return webClient.get(baseUrl + "/blocks/latestBlockTime?utcTime=" + timestamp, Long.class);
  }
}
