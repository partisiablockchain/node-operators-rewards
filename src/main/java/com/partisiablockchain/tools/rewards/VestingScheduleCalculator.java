package com.partisiablockchain.tools.rewards;

/** Calculator containing the same logic as InitialVestingSchedule in the account plugin. */
final class VestingScheduleCalculator {
  /** Start time of pausing the release of vested presale tokens - corresponds to 2023-05-20. */
  static final long PAUSE_RELEASE_START_TIME = 1684540800000L;

  /** End time of pausing the release of vested presale tokens - corresponds to 2024-03-20. */
  static final long PAUSE_RELEASE_END_TIME = 1710892800000L;

  /** Release duration for public token sale. */
  static final long PUBLIC_SALE_RELEASE_DURATION = 62899200000L;

  /** Release interval for public token sale. */
  static final long PUBLIC_SALE_RELEASE_INTERVAL = 7862400000L;

  public final long tokens;
  public final long releasedTokens;
  public final long tokenGenerationEvent;
  public final long releaseDuration;
  public final long releaseInterval;

  VestingScheduleCalculator(
      long tokens,
      long releasedTokens,
      long tokenGenerationEvent,
      long releaseDuration,
      long releaseInterval) {
    this.tokens = tokens;
    this.releasedTokens = releasedTokens;
    this.tokenGenerationEvent = tokenGenerationEvent;
    this.releaseDuration = releaseDuration;
    this.releaseInterval = releaseInterval;
  }

  /**
   * Calculates the amount of vested tokens ready for release.
   *
   * <p>The release of vested presale tokens are paused in the run-up to a potential exchange
   * launch. Token buyers during public sale at TGE are not affected by this.
   *
   * @param blockProductionTime block production time
   * @return amount of vested tokens ready for release
   */
  long getAmountToRelease(long blockProductionTime) {
    if (!isPublicSale()) {
      blockProductionTime = subtractPause(blockProductionTime);
    }
    if (blockProductionTime >= tokenGenerationEvent + releaseDuration) {
      return tokens - releasedTokens;
    } else {
      long amountToRelease = tokens / (releaseDuration / releaseInterval);
      long batches = Math.max(0, (blockProductionTime - tokenGenerationEvent) / releaseInterval);
      long totalToRelease = amountToRelease * batches;

      return totalToRelease - releasedTokens;
    }
  }

  /**
   * Checks if the vesting schedule is from a public sale.
   *
   * @return true if vesting schedule is from a public sale otherwise false
   */
  private boolean isPublicSale() {
    return releaseDuration == PUBLIC_SALE_RELEASE_DURATION
        && releaseInterval == PUBLIC_SALE_RELEASE_INTERVAL;
  }

  /**
   * Subtracts the pause from the block production time to avoid including it when calculating the
   * amount of vested tokens to release.
   *
   * <p>Depending on if the current block production time is before, during, or after the pause
   * duration the time is adjusted as follows:
   *
   * <ul>
   *   <li>If blockProductionTime < {@link #PAUSE_RELEASE_START_TIME} return blockProductionTime
   *   <li>If blockProductionTime < {@link #PAUSE_RELEASE_END_TIME} return {@link
   *       #PAUSE_RELEASE_START_TIME}
   *   <li>If blockProductionTime > {@link #PAUSE_RELEASE_END_TIME} return blockProductionTime -
   *       ({@link #PAUSE_RELEASE_END_TIME} - {@link #PAUSE_RELEASE_START_TIME})
   * </ul>
   *
   * @param blockProductionTime block production time
   * @return block production time adjusted for pause duration
   */
  static long subtractPause(long blockProductionTime) {
    return Math.max(
        Math.min(PAUSE_RELEASE_START_TIME, blockProductionTime),
        blockProductionTime - (PAUSE_RELEASE_END_TIME - PAUSE_RELEASE_START_TIME));
  }
}
