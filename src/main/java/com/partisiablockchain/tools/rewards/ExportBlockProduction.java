package com.partisiablockchain.tools.rewards;

import com.fasterxml.jackson.databind.JsonNode;
import com.partisiablockchain.client.ApiChainClient;
import com.partisiablockchain.dto.BlockState;
import com.partisiablockchain.dto.traversal.TraversePath;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.thread.ExecutorFactory;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

class ExportBlockProduction {

  static final File OUTPUT_DIR = new File("mainnet/LedgerData used for node performance");
  private final String shard;
  private final ApiChainClient client;
  private final JsonNode committees;

  ExportBlockProduction(String shard) {
    this.shard = shard;
    this.client = getPbcHttpClient(shard);
    JsonNode jsonNode = client.traverseGlobalConsensusPlugin(new TraversePath(List.of()));
    this.committees = jsonNode.get("committees");
  }

  static void exportMonth(String month) throws InterruptedException, ExecutionException {
    ExecutorService export = ExecutorFactory.newCached("Export");
    try {
      ExportBlockProduction gov = new ExportBlockProduction("Gov");
      ExportBlockProduction shard0 = new ExportBlockProduction("Shard0");
      ExportBlockProduction shard1 = new ExportBlockProduction("Shard1");
      ExportBlockProduction shard2 = new ExportBlockProduction("Shard2");
      List<ExportBlockProduction> shards = List.of(gov, shard0, shard1, shard2);
      System.out.println("Exporting for month " + month);
      List<Future<Object>> futures =
          export.invokeAll(
              shards.stream().map(s -> Executors.callable(() -> s.printForShard(month))).toList());
      for (Future<Object> future : futures) {
        future.get();
      }
    } finally {
      export.shutdownNow();
    }
  }

  private void printForShard(String exportMonth) {
    YearMonth month = YearMonth.parse(exportMonth);
    DateTimeFormatter formatter =
        DateTimeFormatter.ofPattern("uuuu-MM-MMMM")
            .withZone(ZoneId.of("UTC"))
            .withLocale(Locale.ENGLISH);
    String suffix = formatter.format(month) + ".csv";
    PrintStream out = ExceptionConverter.call(() -> getPrintStream(suffix));
    if (out != null) {
      System.out.println("Export month " + exportMonth + " for shard " + shard);
      LocalDate localDate = month.atDay(1);
      LocalDateTime initial = localDate.atStartOfDay();
      LocalDateTime nextMonth =
          localDate.with(TemporalAdjusters.firstDayOfNextMonth()).atStartOfDay();

      final long firstBlock =
          ExportStakes.getLatestBlockTimeBefore(
                  shard, initial.toInstant(ZoneOffset.UTC).toEpochMilli())
              + 1;
      Instant end = nextMonth.toInstant(ZoneOffset.UTC);
      if (Instant.now().isBefore(end)) {
        throw new RuntimeException("Unable to export a month before it has ended.");
      }
      final long lastBlock = ExportStakes.getLatestBlockTimeBefore(shard, end.toEpochMilli());

      for (long i = firstBlock; i <= lastBlock; i++) {
        BlockState block = getBlock(i);
        printBlock(block, out);
      }
      System.out.println("Finished exporting month " + exportMonth + " for shard " + shard);
    } else {
      System.out.println("Ignoring already exported month " + exportMonth + " for shard " + shard);
    }
  }

  private PrintStream getPrintStream(String suffix) throws FileNotFoundException {
    String fileName = "export-" + shard + "-" + suffix;
    File file = new File(OUTPUT_DIR, fileName);
    if (file.exists()) {
      // Do not overwrite
      return null;
    }
    PrintStream printStream = new PrintStream(file);
    printStream.println(
        "blockNumber;committee;producerIdx;producerAddress;productionTime;productionTimeHRF");
    return printStream;
  }

  private BlockState getBlock(long blockNumber) {
    try {
      return client.getBlockByNumber(blockNumber);
    } catch (Exception e) {
      return client.getBlockByNumber(blockNumber);
    }
  }

  private ApiChainClient getPbcHttpClient(String shard) {
    if (Objects.equals(shard, "Gov")) {
      return ApiChainClient.create("https://reader.partisiablockchain.com");
    } else {
      return ApiChainClient.create("https://reader.partisiablockchain.com/shards/" + shard);
    }
  }

  private void printBlock(BlockState blockByNumber, PrintStream printStream) {
    long productionTime = blockByNumber.productionTime();
    Instant instant = Instant.ofEpochMilli(productionTime);
    JsonNode committeeList = committees.get((int) blockByNumber.committeeId());
    JsonNode activeCommittee = committeeList.get("activeCommittee");
    JsonNode jsonNode = activeCommittee.get(blockByNumber.producer());
    String producer;
    if (jsonNode != null) {
      producer = jsonNode.get("identity").toString();
    } else {
      producer = "RESET";
    }
    printStream.printf(
        "%d;%d;%d;%s;%d;%s\n",
        blockByNumber.blockTime(),
        blockByNumber.committeeId(),
        blockByNumber.producer(),
        producer,
        blockByNumber.productionTime(),
        instant.toString());
  }
}
