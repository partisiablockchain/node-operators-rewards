package com.partisiablockchain.tools.rewards;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.LongSummaryStatistics;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/** Calculate BP scores based on ledger data. */
public final class BpScore {

  private static final List<String> SHARDS = List.of("Gov", "Shard0", "Shard1", "Shard2");

  static void calculateScoreForMonth(long monthIndex) throws Exception {
    YearMonth firstMonth = YearMonth.parse("2022-06");
    YearMonth exportMonth = firstMonth.plusMonths(monthIndex - 1);

    final File target =
        new File("mainnet/results/Performance scores", "m" + monthIndex + "_score.csv");
    if (target.exists()) {
      System.out.println(
          "Ignoring calculation of BP score since it already exist for month " + exportMonth);
      return;
    }
    if (monthIndex <= 9) {
      // Exported values from Java and R do not have exact same precision. Only allow Java to handle
      // the months after the new script was available.
      throw new RuntimeException("First 3 quarters should be calculated using R-script");
    }
    ExportBlockProduction.exportMonth(exportMonth.toString());

    LinkedHashMap<String, Long> allNodes = readProducedBlocks(exportMonth);

    Set<String> producersInPreviousMonth = readProducedBlocks(exportMonth.minusMonths(1)).keySet();

    LinkedHashMap<String, Long> existingNodes = new LinkedHashMap<>();
    LinkedHashMap<String, Long> newNodes = new LinkedHashMap<>();
    for (String s : allNodes.keySet()) {
      if (producersInPreviousMonth.contains(s)) {
        existingNodes.put(s, allNodes.get(s));
      } else {
        newNodes.put(s, allNodes.get(s));
      }
    }

    LongSummaryStatistics longSummaryStatistics =
        existingNodes.values().stream().mapToLong(Long::longValue).summaryStatistics();
    long max = longSummaryStatistics.getMax();

    PrintStream printStream = new PrintStream(target);

    printStream.println("address;no_blocks;score_m" + monthIndex);
    double sumOfScores = 0;
    for (Map.Entry<String, Long> entry : existingNodes.entrySet()) {
      String producer = entry.getKey();
      Long blocks = entry.getValue();
      double score = blocks.doubleValue() / max;
      sumOfScores += score;

      printStream.println(producer + ";" + blocks + ";" + score);
    }

    double averageScore = sumOfScores / existingNodes.size();

    for (Map.Entry<String, Long> entry : newNodes.entrySet()) {
      String producer = entry.getKey();
      Long blocks = entry.getValue();

      printStream.println(producer + ";" + blocks + ";" + averageScore);
    }
  }

  private static LinkedHashMap<String, Long> readProducedBlocks(YearMonth exportMonth)
      throws IOException {
    // Linked to match behaviour of old script
    LinkedHashMap<String, Long> map = new LinkedHashMap<>();
    for (String shard : SHARDS) {
      DateTimeFormatter formatter =
          DateTimeFormatter.ofPattern("uuuu-MM-MMMM")
              .withZone(ZoneId.of("UTC"))
              .withLocale(Locale.ENGLISH);
      String suffix = formatter.format(exportMonth) + ".csv";
      String fileName = "export-" + shard + "-" + suffix;
      File file = new File(ExportBlockProduction.OUTPUT_DIR, fileName);
      List<String> strings = Files.readAllLines(file.toPath());
      strings = strings.subList(1, strings.size());
      for (String string : strings) {
        String[] split = string.split(";", -1);
        String producer = split[3].replace("\"", "");
        map.compute(producer, (key, value) -> Objects.requireNonNullElse(value, 0L) + 1L);
      }
    }

    map.remove("RESET");
    return map;
  }
}
